#include <conio.h>
#include <process.h>
#include <sched.h>
#include <sema.h>
#include <string.h>
#include <fileio.h>

int main(int argc, char** argv) {
    int pid = Get_PID();
    Kill(pid, 1);
    Print("It failed.\n");
    return 0;
}

