#include <conio.h>
#include <process.h>
#include <sched.h>
#include <sema.h>
#include <string.h>
#include <fileio.h>

void wait1() {
    int i = 0;
    float f = 100.000f;
    for (i = 0; i < 1000000; i++) {
        if(i % 20 == 0) {
          Print("");  
        }
        f = i / f;
    }
}

void signal_user1(int sig) {
    
}

int main(int argc, char** argv) {
    int pid = Get_PID();
    
    Print("Signal Test 2: %2d\n", pid);
    
    while(true) {
        wait1();
    }
    
    return 0;
}