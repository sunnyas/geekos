#include <conio.h>
#include <process.h>
#include <sched.h>
#include <sema.h>
#include <string.h>
#include <fileio.h>

void sig_child(int sig) {
    int pid = Get_PID();
    int status = 0;
    int child = 0;
    // Print("Got a SIGCHILD. I am: %2d\n", pid);
    child = WaitNoPID(&status);
    Print("Parent: %2d , Child: %2d , ExitStat: %3d\n", pid, child, status);
}

void wait1() {
    int i = 0;
    float f = 100.000f;
    for (i = 0; i < 10000000; i++) {
        if (i % 20 == 0) {
            Print("");
        }
        f = i / f;
    }
}

int main(int argc, char** argv) {
    int pid;
    int pid1;
    int pid2;
    int fork;

    Signal(sig_child, 4);

    fork = Fork();
    wait1();

    if (fork) {
        pid = Get_PID();
        Print("Parent: %2d Child: %2d\n", pid, fork);
    } else {
        pid = Get_PID();
        Print("Child: %2d\n", pid);
    }

    pid1 = Spawn_Program("/c/signal1.exe","/c/signal1.exe",0);
    pid2 = Spawn_Program("/c/signal1.exe","/c/signal1.exe",0);
    wait1();
    Kill(pid1, 2);
    Kill(pid2, 2);
    wait1();
    Kill(pid1, 1);
    Kill(pid2, 1);
    wait1();
    
    if(fork) {
        Kill(fork, 1);
        wait1();
    }

    Kill(pid, 1);
    Print("It failed.\n");
    return 0;
}
