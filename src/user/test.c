#include <conio.h>
#include <process.h>
#include <sched.h>
#include <sema.h>
#include <string.h>
#include <fileio.h>


int main(int argc, char ** argv) {
    int childPid;
    for(;;){
        childPid = Fork();
        if(childPid == 0) {
            break;
        }
        Print("Parent: pid:%d Child: %d\n", Get_PID(), childPid);
    }
    Print("Child: pid:%d\t", Get_PID());
    return 0;
}
