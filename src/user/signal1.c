#include <conio.h>
#include <process.h>
#include <sched.h>
#include <sema.h>
#include <string.h>
#include <fileio.h>

int lastSig = 0;

void wait1() {
    int i = 0;
    float f = 100.000f;
    for (i = 0; i < 1000000; i++) {
        if(i % 20 == 0) {
          Print("");  
        }
        f = i / f;
    }
}

void signal_user1(int sig) {
    int pid = Get_PID();
    static int reenter = 0;
    lastSig = sig;
    Print("Signal-1: pid = %2d sig = %2d\n", pid, sig);
    if(!reenter) {
        Kill(pid, sig);
        wait1();
        Print("Signal-1: Resent Same pid = %2d sig = %2d\n", pid, sig);
        wait1();
        reenter++;
    } else {
        Kill(pid, 3);
        wait1();
        Print("Signal-1: Resent Diff pid = %2d sig = %2d\n", pid, sig);
        wait1();
    }
    Print("Signal-1: Handled pid = %2d sig = %2d\n", pid, sig);
}

void signal_user2(int sig) {
    int pid = Get_PID();
    lastSig = sig;
    Print("Signal-1: pid = %2d sig = %2d\n", pid, sig);
}

int main(int argc, char** argv) {
    int pid = Get_PID();
    int sig = 0;
    
    Signal(signal_user1, 2);
    Signal(signal_user2, 3);
    
    Print("Signal-1: Start. pid = %2d\n", pid);
    
    while(true) {
        if(lastSig != sig) {
            Print("Jump back. pid = %2d lastSig = %2d\n",pid,lastSig);
            sig = lastSig;
        }
        wait1();
    }
    
    return 0;
}