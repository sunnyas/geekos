/*************************************************************************/
/*
 * GeekOS master source distribution and/or project solution
 * Copyright (c) 2001,2003,2004 David H. Hovemeyer <daveho@cs.umd.edu>
 * Copyright (c) 2003 Jeffrey K. Hollingsworth <hollings@cs.umd.edu>
 *
 * This file is not distributed under the standard GeekOS license.
 * Publication or redistribution of this file without permission of
 * the author(s) is prohibited.
 */
/*************************************************************************/
/*
 * Signals
 * $Rev $
 * 
 * This is free software.  You are permitted to use,
 * redistribute, and modify it as specified in the file "COPYING".
 */

#include <geekos/kassert.h>
#include <geekos/defs.h>
#include <geekos/screen.h>
#include <geekos/int.h>
#include <geekos/mem.h>
#include <geekos/symbol.h>
#include <geekos/string.h>
#include <geekos/kthread.h>
#include <geekos/malloc.h>
#include <geekos/user.h>
#include <geekos/signal.h>
#include <geekos/projects.h>


void Send_Signal(struct Kernel_Thread *kthread, int signal) {
    if (kthread == 0 || kthread->userContext == 0)
        return;
    kthread->userContext->sigFlags[signal] = 1;
}

/* Called when signal handling is complete. */
void Complete_Handler(struct Kernel_Thread *kthread,
                      struct Interrupt_State *state) {
    KASSERT(kthread);
    KASSERT(state);
    
    struct User_Interrupt_State * uState = (struct User_Interrupt_State *)state;
    struct User_Context * context = kthread->userContext;
    ulong_t userStack = context->memory + uState->espUser;
    
    if((*(int *)userStack) == SIGCHLD){
        // stub for testing
        int j;
    }
    
    userStack += sizeof(int);
    memcpy(uState, userStack, sizeof(*uState));
    
    context->handling = 0;
    
    // check it if matters...it shouldn't
    // userStack += sizeof(*uState);
    // uState->espUser = userStack - (ulong_t)context->memory;
    
    //TODO_P(PROJECT_SIGNALS,
    //       "Complete_Handler cleans up after a signal handler");
}

int Check_Pending_Signal(struct Kernel_Thread *kthread,
                         struct Interrupt_State *state) {
    KASSERT(kthread);
    KASSERT(state);

    //TODO_P(PROJECT_SIGNALS,
    //     "Check_Pending_Signal returns 1 if this thread has a pending signal");
    
    if (kthread->userContext->handling || KERNEL_CS == state->cs) {
        goto exit;
    }
    
    int i = 1;
    for(;i < MAXSIG + 1; i++) {
        if(kthread->userContext->sigFlags[i]) {
            return 1;
        }
    }
    exit:
    return 0;
}

#if 0
void Print_IS(struct Interrupt_State *esp) {
    void **p;
    Print("esp=%x:\n", (unsigned int)esp);
    Print("  gs=%x\n", (unsigned int)esp->gs);
    Print("  fs=%x\n", (unsigned int)esp->fs);
    Print("  es=%x\n", (unsigned int)esp->es);
    Print("  ds=%x\n", (unsigned int)esp->ds);
    Print("  ebp=%x\n", (unsigned int)esp->ebp);
    Print("  edi=%x\n", (unsigned int)esp->edi);
    Print("  esi=%x\n", (unsigned int)esp->esi);
    Print("  edx=%x\n", (unsigned int)esp->edx);
    Print("  ecx=%x\n", (unsigned int)esp->ecx);
    Print("  ebx=%x\n", (unsigned int)esp->ebx);
    Print("  eax=%x\n", (unsigned int)esp->eax);
    Print("  intNum=%x\n", (unsigned int)esp->intNum);
    Print("  errorCode=%x\n", (unsigned int)esp->errorCode);
    Print("  eip=%x\n", (unsigned int)esp->eip);
    Print("  cs=%x\n", (unsigned int)esp->cs);
    Print("  eflags=%x\n", (unsigned int)esp->eflags);
    p = (void **)(((struct Interrupt_State *)esp) + 1);
    Print("esp+n=%x\n", (unsigned int)p);
    Print("esp+n[0]=%x\n", (unsigned int)p[0]);
    Print("esp+n[1]=%x\n", (unsigned int)p[1]);
}

void dump_stack(unsigned int *esp, unsigned int ofs) {
    int i;
    Print("Setup_Frame: Stack dump\n");
    for (i = 0; i < 25; i++) {
        Print("[%x]: %x\n", (unsigned int)&esp[i] - ofs, esp[i]);
    }
}
#endif

void Setup_Frame(struct Kernel_Thread *kthread, struct Interrupt_State *state) {
    int i = 1;
    int signal = 0;
    KASSERT(kthread);
    KASSERT(state);
    
    struct User_Interrupt_State * uState = (struct User_Interrupt_State *)state;
    struct User_Context * context = kthread->userContext;
    ulong_t userStack = context->memory + uState->espUser;
    signal_handler sigH = 0;
    
    for(;i < MAXSIG + 1; i++) {
        if(context->sigFlags[i]) {
            signal = i;
            sigH = context->handlers[i];
            context->sigFlags[i] = 0;
            break;
        }
    }
    
    if (sigH == SIG_DFL) {
        Print("Terminated %d.\n", g_currentThread->pid);
        Exit(0);
    } else if (sigH == SIG_IGN) {
        return;
    }
    
    if(signal == SIGCHLD) {
        // stub for testing
        int j;
    }
    
    context->handling = 1;
    
    userStack -= sizeof(*uState);
    memcpy(userStack, uState, sizeof(*uState));
    userStack -= sizeof(int);
    memcpy(userStack, &signal, sizeof(int));
    userStack -= sizeof(uint_t);
    memcpy(userStack, &context->trampoline, sizeof(uint_t));
    
    uState->espUser = userStack - (ulong_t)context->memory;
    state->eip = sigH;

    // TODO_P(PROJECT_SIGNALS, "Setup_Frame");
}
