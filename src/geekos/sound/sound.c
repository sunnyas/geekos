
#include <geekos/io.h>
#include <geekos/int.h>
#include <geekos/irq.h>
#include <geekos/dma.h>
#include <geekos/malloc.h>
#include <geekos/errno.h>
#include <geekos/kassert.h>
#include <geekos/kthread.h>
#include <geekos/list.h>
#include <geekos/timer.h>
#include <geekos/alarm.h>
#include <geekos/mem.h>

#include <geekos/vfs.h>

#include <geekos/projects.h>

#include <geekos/string.h>
#include <geekos/sound.h>

//#define DEBUG_SOUND
#ifdef DEBUG_SOUND
#  define Debug(args...) Print(args)
#else
#  define Debug(args...)
#endif

#define SOUND_BASE       0x220
#define SOUND_DSP_RESET  (SOUND_BASE + 0x6)

#define SOUND_READ_DATA  (SOUND_BASE + 0xA)
#define SOUND_READ_RDY   (SOUND_BASE + 0xE)

#define SOUND_WRITE_RDY  (SOUND_BASE + 0xC)
#define SOUND_WRITE_DATA (SOUND_BASE + 0xC)

#define SOUND_IRQ        5


struct Sound_Header {
    uint_t chunk_id, chunk_size, format, subchunk1_id, subchunk1_size;
    ushort_t audio_format, num_channel;
    uint_t sample_rate, byte_rate;
    ushort_t block_align, bits_per_sample;
    uint_t subchunk2_id, subchunk2_size;
} headerStruct;
struct Sound_Header *header = &headerStruct;
struct File *wave_file = 0;

// Default DMA Channel 1 (for 8 bit)
int dma_channel = 1;

void * buf = 0x100000 + 8192 + 0xE000;
long buf_size = 65536;

// Gives us 2 extra buffers (192KB read before playing)
// After 192 KB its similar to a triple buffer. (If I had threads)
void * read_buf = 0;
long read_buf_size = 65536 * 2;

// Handlers in where in the file we are (start of the 2nd buffer is 0)
long last_read = 0;
long last_chunk = 0;

// End conditional variable (Stops extra interrupts just incase)
bool finished = true;

// Checks for read/write status bits on DSP
static void Wait_For_DSP(bool read) {
    if(read) {
        while(!(In_Byte(SOUND_READ_RDY) & 0x80));
    } else {
        while(In_Byte(SOUND_WRITE_RDY) & 0x80);
    }
}

// Closes the file, cleans up memory, and tells DMA to stop
void Finish_Sound() {
    if(finished)
        return;
    finished = true;
    Debug("End DMA after current block.\n");
    
    // Send DMA Stop after Block Signal
    Wait_For_DSP(false);
    if(header->bits_per_sample == 8) {
        Out_Byte(SOUND_WRITE_DATA, 0xDA);
    } else {
        Out_Byte(SOUND_WRITE_DATA, 0xD9);
    }
    // Disable Interrupts on IRQ 5
    Disable_IRQ(5);
    // Clean up
    Close(wave_file);
    wave_file = NULL;
    Free(read_buf);
    read_buf = NULL;
}

// Handler for sound interrupts
void Sound_Interrupt_Handler(struct Interrupt_State * state) {
    if(finished) {
        return;
    }
    Begin_IRQ(state);
    // Keep track of # of interrupts called.
    static int called = 0;
    // Chunk read by DMA and its size 
    int chunk = called % 2;
    int chunk_size = buf_size / 2;
    // Amount to copy from our other 2 buffers
    int read_size = 0;
    if (last_read - last_chunk < chunk_size) {
        read_size = last_read - last_chunk;
    } else {
        read_size = chunk_size;
    }

    Debug("Check Point G: %d\n", read_size);
    memcpy(buf + chunk*chunk_size,read_buf + (called % 4)*chunk_size,chunk_size);
    // End conditions (End if nothing left, 0 out extra buffer if old data left)
    if(read_size <= 0) {
        Finish_Sound();
    } else if (read_size < chunk_size) {
        memset(buf + chunk*chunk_size + read_size, 0, chunk_size-read_size);
    }
    
    // Acknowledge Interrupt after updating buffers
    In_Byte(header->bits_per_sample == 8 ? 0x22E : 0x22F);
    
    Debug("Check Point J: %d:%d->%d\n",called,last_chunk,last_chunk+chunk_size);
    
    // Updates interrupt # and chunks already loaded into buffer
    called++;
    last_chunk += chunk_size;
    // Read into 2nd and third buffers after they are used up
    if((called) % 2 == 0) {
        // 2nd buffer (0) or 3rd buffer (1) to be read into.
        int half = !(((called)/2)%2);
        Debug("Check Point K: %d->%d:%d\n", called, half,last_read);
        // May end up calling this function again so all states are updated
        Enable_Interrupts();
        last_read += 
          Read(wave_file, read_buf + half*read_buf_size / 2, read_buf_size / 2);
        Disable_Interrupts();
    }
    End_IRQ(state);
}
 
void SB16_Play_File(const char *filename) {
    int read_size = 0;
    Open(filename, O_READ, &wave_file);
    read_buf = Malloc(read_buf_size);
    
    // Read header info
    Read(wave_file, header, sizeof(struct Sound_Header));
    // Fill DMA buffer
    read_size = Read(wave_file, buf, buf_size);
    last_read = -1 * read_size;
    // Read into extra read buffer
    if(read_size == buf_size) {
        last_read = Read(wave_file, read_buf, read_buf_size);
    }
    finished = false;
    
    Debug("Check Point A: %d,%d\n", read_size, last_read);
    
    // compute modes and channels based on wave file
    // based on SoundBlaster.pdf 3-28
    uchar_t b_command;
    uchar_t b_mode;
    uint_t num_samples = buf_size / 2;
    if(header->bits_per_sample == 8) {
        b_command = 0xC6;
        b_mode = 0x00;
    } else {
        num_samples = num_samples / 2;
        dma_channel = 5;
        b_command = 0xB6;
        b_mode = 0x10;
    }
    
    // b_mode of stereo is different (set 10th bit to 1)
    if(header->num_channel == 2) {
        b_mode = b_mode | 0x20;
    }
    
    Install_IRQ(SOUND_IRQ, &Sound_Interrupt_Handler);
    Enable_IRQ(SOUND_IRQ);
    
    // Setup_DMA requires this (floppy.c has it)
    Disable_Interrupts();
    if (!Reserve_DMA(dma_channel)) {
        Print("Failed to reserve DMA channel %d\n", dma_channel);
        Close(wave_file);
        Enable_Interrupts();
        return;
    }
    // Full buffer if DMA was filled, only read amount if not.
    // Derived from 3-28 in SoundBlaster.pdf
    Setup_DMA(DMA_WRITE, dma_channel, buf, 
        (read_size < buf_size ? read_size : buf_size));
    Enable_Interrupts();
    
    // Setup Sampling Rate
    Wait_For_DSP(false);
    Out_Byte(SOUND_WRITE_DATA, 0x41);
    Wait_For_DSP(false);
    Out_Byte(SOUND_WRITE_DATA, header->sample_rate >> 8);
    Wait_For_DSP(false);
    Out_Byte(SOUND_WRITE_DATA, header->sample_rate & 0xff);
    
    // write in the modes and the number of sample per chunk size
    Wait_For_DSP(false);
    Out_Byte(SOUND_WRITE_DATA, b_command);
    Wait_For_DSP(false);
    Out_Byte(SOUND_WRITE_DATA, b_mode);
    Wait_For_DSP(false);
    Out_Byte(SOUND_WRITE_DATA, (num_samples - 1) & 0xff);
    Wait_For_DSP(false);
    Out_Byte(SOUND_WRITE_DATA, (num_samples - 1) >> 8);
    // Sample number -1 is passed as said in SoundBlaster.pdf 3-29 (wBlkSize)
    
    if(read_size < buf_size) {
        Finish_Sound();
        Debug("Too Short Exit Condition.\n");
    }
    Debug("Check Point F.\n");
    //TODO_P(PROJECT_SOUND, "Play a named file");
}

void Init_Sound_Devices(void) {
    // Version major and minor
    int vMajor, vMinor;
    
    Out_Byte(SOUND_DSP_RESET, 1);
    Micro_Delay(300);
    Out_Byte(SOUND_DSP_RESET, 0);
    
    Wait_For_DSP(true);
    if(In_Byte(SOUND_READ_DATA) == 0xAA) {
        Wait_For_DSP(false);
        Out_Byte(SOUND_WRITE_DATA, 0xE1);
        Wait_For_DSP(true);
        vMajor = In_Byte(SOUND_READ_DATA);
        Wait_For_DSP(true);
        vMinor = In_Byte(SOUND_READ_DATA);
        Print("SB16 found: version %d.%d\n", vMajor, vMinor);
    } else {
        Print("SB16 not found\n");
    }
    // TODO_P(PROJECT_SOUND, "Initialize sound card");
}
