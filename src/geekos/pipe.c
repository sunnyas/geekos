#define GEEKOS

#include <geekos/pipe.h>
#include <geekos/malloc.h>
#include <geekos/string.h>
#include <geekos/errno.h>
#include <geekos/projects.h>

#define MAX_BUFFER_SIZE 32768

struct Pipe {
    uint_t readers, writers; // as booleans
    uint_t curr_pos, end_pos;
    char * buf;
};

struct File_Ops Pipe_Read_Ops =
    { NULL, Pipe_Read, NULL, NULL, Pipe_Close, NULL };
struct File_Ops Pipe_Write_Ops =
    { NULL, NULL, Pipe_Write, NULL, Pipe_Close, NULL };

int Pipe_Create(struct File **read_file, struct File **write_file) {
    struct File *read, *write;
    struct Pipe *pipe;
    read = (struct File *)Malloc(sizeof(struct File));
    if(read <= 0) {
        return ENOMEM;
    }
    write = (struct File *)Malloc(sizeof(struct File));
    if(write <= 0) {
        Free(read);
        return ENOMEM;
    }
    pipe = (struct Pipe *)Malloc(sizeof(struct Pipe));
    if(pipe <= 0) {
        Free(read);
        Free(write);
        return ENOMEM;
    }
    
    pipe->buf = NULL;
    pipe->curr_pos = 0;
    pipe->end_pos = 0;
    pipe->readers = 1;
    pipe->writers = 1;
    
    read->ops = &Pipe_Read_Ops;
    write->ops = &Pipe_Write_Ops;
    
    read->refCount = 1;
    write->refCount = 1;
    
    read->endPos = 0;
    read->filePos = 0;
    read->fsData = pipe;
    write->endPos = 0;
    write->filePos = 0;
    write->fsData = pipe;
    
    *read_file = read;
    *write_file = write;
    
    return 0;
    
    //TODO_P(PROJECT_PIPE, "Create a pipe");
}

int Pipe_Read(struct File *f, void *buf, ulong_t numBytes) {
    struct Pipe *pipe = (struct Pipe *)f->fsData;
    char * buffer = (char *) buf;
    if( (pipe->buf == NULL || pipe->curr_pos == pipe->end_pos) && pipe->writers > 0) {
        return EWOULDBLOCK;
    }
    if(pipe->buf == NULL) {
        return 0;
    }
    int i = 0;
    for(i = 0; i < numBytes; i++) {
        if(pipe->curr_pos == pipe->end_pos) {
            break;
        }
        buffer[i] = pipe->buf[pipe->curr_pos];
        pipe->curr_pos = (pipe->curr_pos + 1) % MAX_BUFFER_SIZE;
    }
    return i;
    // TODO_P(PROJECT_PIPE, "Pipe read");
}

int Pipe_Write(struct File *f, void *buf, ulong_t numBytes) {
    struct Pipe *pipe = (struct Pipe *)f->fsData;
    char * buffer = (char *) buf;
    if(pipe->readers == 0) {
        return EPIPE;
    }
    if(pipe->buf == NULL) {
        pipe->buf = (char *)Malloc(MAX_BUFFER_SIZE);
        if(pipe->buf == NULL) {
            return ENOMEM;
        }
    }
    int i = 0;
    for(i = 0; i < numBytes; i++) {
        if(pipe->end_pos + 1 % MAX_BUFFER_SIZE == pipe->curr_pos) {
            break; //circular buffer full
        }
        pipe->buf[pipe->end_pos] = buffer[i];
        pipe->end_pos = (pipe->end_pos + 1) % MAX_BUFFER_SIZE;
    }
    
    return i;
    // TODO_P(PROJECT_PIPE, "Pipe write");
}

int Pipe_Close(struct File *f) {
    struct Pipe *pipe = (struct Pipe *)f->fsData;
    if(f->ops == &Pipe_Read_Ops) {
        if(f->refCount == 1) {
            pipe->readers--;
        }
        if(pipe->readers == 0) {
            Free(pipe->buf);
        }
    } else if (f->ops == &Pipe_Write_Ops && f->refCount == 1) {
        pipe->writers--;
    }
    if(pipe->readers == 0 && pipe->writers == 0) {
        Free(pipe);
    }
    // TODO_P(PROJECT_PIPE, "Pipe close");
    return 0;
}
